﻿using UnityEngine;
using System.Collections;

public class AnimationLauncher : MonoBehaviour {

	public Animator animator;

	public void DoAnimation1() {
		animator.SetTrigger("Animation1");
	}

	public void DoAnimation2() {
		animator.SetTrigger("Animation2");
	}

	public void DoAnimation3() {
		animator.SetTrigger("Animation3");
	}

}
